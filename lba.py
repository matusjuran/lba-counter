import time

class Automaton	:

	def __init__(self,length):
		self.length = length
		self.tape = ['0' for _ in range(length+2)]
		self.tape[0] = 'c'
		self.tape[length+1] = '$'
		self.pos = 1
		self.state = 'RIGHT'
		self.delta = {}

		self.delta[('SEEK','c')] = ('END','c',0)
		self.delta[('SEEK','$')] = ('SEEK','$',-1)
		self.delta[('SEEK','1')] = ('SEEK','1',-1)
		self.delta[('SEEK','0')] = ('RIGHT','1',1)

		self.delta[('RIGHT','c')] = ('RIGHT','c',1)
		self.delta[('RIGHT','$')] = ('NUM','$',0)
		self.delta[('RIGHT','1')] = ('RIGHT','0',1)
		self.delta[('RIGHT','0')] = ('RIGHT','0',1)

		self.delta[('NUM','$')] = ('SEEK','$',0)
		
	def compute(self):
		print(self.visualize())
		delta_res = self.delta[(self.state,self.tape[self.pos])]
		self.tape[self.pos] = delta_res[1]
		self.pos += delta_res[2]
		self.state = delta_res[0]
		if self.state == 'END':
			return
		time.sleep(1)
		self.compute()
	
	def visualize(self):
		res = ''
		for cell in self.tape:
			res += cell
		res += ' (' + self.state + ')'
		if self.state == 'NUM':
			# green entire tape + state
			return '\33[32m' + res + '\33[0m'
		else:
			# red current cell
			return res[:self.pos] + '\33[31m' + res[self.pos] + '\33[0m' + res[self.pos+1:]
