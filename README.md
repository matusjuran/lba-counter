# Linear bounded counter
A linear bounded automaton is able to generate all the strings of the same length as the input over the tape alphabet in lexicographical order.
A generalization of this technique is used, for example, in the proof of the Szelepcsényi-Immerman theorem.
This simple console application visualizes an automaton generating all the binary digits of the length specified by the user.
It can be run using `python3 simulation.py`.
